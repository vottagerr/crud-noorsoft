import React, { useState } from "react";
import axios from "axios";

const Users = ({ pos, UserData, onDelete }) => {
  const [status, setStatus] = useState(true);

  const [data, setData] = useState(UserData.data);

  const EditData = (_id) => {
    axios
      .post(`http://178.128.196.163:3000/api/records/${_id}`, {
        data,
      })
      .then(() => {
        setStatus(true);
      });
  };

  return (
    <tr key={pos}>
      <td>
        <input
          className="input-user"
          name="name"
          disabled={status}
          type="text"
          value={data.name !== "" ? data.name : "нет данных"}
          onChange={(e) =>
            setData({ ...data, [e.target.name]: e.target.value })
          }
        ></input>
      </td>
      <td>
        <input
          name="age"
          disabled={status}
          type="text"
          value={data.age !== "" ? data.age : "нет данных"}
          onChange={(e) =>
            setData({ ...data, [e.target.name]: e.target.value })
          }
        ></input>
      </td>
      <td>
        <input
          name="email"
          disabled={status}
          type="text"
          value={data.email !== "" ? data.email : "нет данных"}
          onChange={(e) =>
            setData({ ...data, [e.target.name]: e.target.value })
          }
        ></input>
      </td>

      <td className="actions-buttons">
        {status ? (
          <button
            onClick={() => {
              setStatus(false);
            }}
            className="btn btn-edit"
          >
            Edit
          </button>
        ) : (
          <button
            onClick={() => {
              EditData(UserData._id);
            }}
            className="btn btn-edit"
          >
            Save
          </button>
        )}

        <button
          onClick={() => onDelete(UserData._id)}
          className="btn btn-delete"
        >
          Delete
        </button>
      </td>
    </tr>
  );
};

export default Users;
