import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import "./AddUser.css";

const AddUser = () => {
  let navigate = useNavigate();

  const [name, setName] = useState("");
  const [age, setAge] = useState("");
  const [email, setEmail] = useState("");

  const postData = (e) => {
    e.preventDefault();
    axios
      .put("http://178.128.196.163:3000/api/records", {
        data: {
          name,
          age,
          email,
        },
      })
      .then(() => {
        navigate("/");
      });
  };

  return (
    <div>
      <header>
        <h3>Add New User</h3>
      </header>

      <form className="form">
        <input
          className="input-add-user"
          type="text"
          name="name"
          placeholder="Name"
          onChange={(e) => setName(e.target.value)}
        />

        <input
          className="input-add-user"
          type="text"
          name="age"
          placeholder="Age"
          onChange={(e) => setAge(e.target.value)}
        />

        <input
          className="input-add-user"
          type="text"
          name="email"
          placeholder="Email"
          onChange={(e) => setEmail(e.target.value)}
        />

        <div className="buttons">
          <button
            onClick={() => {
              navigate("/");
            }}
            className="btn btn-modal"
          >
            Back
          </button>

          {/* <Link to={"/"}> */}
          <button
            type="submit"
            onClick={(e) => postData(e)}
            className="btn btn-modal"
          >
            Submit
          </button>
          {/* </Link> */}
        </div>
      </form>
    </div>
  );
};

export default AddUser;
