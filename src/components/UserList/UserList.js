import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Users from "../Users/Users";

import "./UserList.css";

const UserList = () => {
  let navigate = useNavigate();

  const [APIData, setAPIData] = useState([]);
  useEffect(() => {
    axios.get(`http://178.128.196.163:3000/api/records`).then((response) => {
      console.log(response.data);
      setAPIData(response.data);
    });
  }, []);

  const getData = () => {
    axios.get(`http://178.128.196.163:3000/api/records`).then((getData) => {
      setAPIData(getData.data);
    });
  };

  const onDelete = (_id) => {
    axios.delete(`http://178.128.196.163:3000/api/records/${_id}`).then(() => {
      getData();
    });
  };

  return (
    <div className="UserList">
      <header>
        <h3>Users List</h3>
      </header>
      <table className="table-users">
        <thead>
          <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Email</th>
            {/* <th>Adress</th> */}
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {APIData.map((UserData, pos) => {
            return UserData.hasOwnProperty("data") ? (
              <Users UserData={UserData} onDelete={onDelete} />
            ) : (
              <tr key={pos}>
                <td>не определено</td>
                <td>не определено</td>
                <td>не определено</td>
                {/* <td>{data.data.adress}</td> */}

                <td className="actions-buttons">
                  <button
                    onClick={() => onDelete(UserData._id)}
                    className="btn btn-delete"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>

      <button
        onClick={() => {
          navigate("/AddUser");
        }}
        className="btn-add"
      >
        Add User
      </button>
    </div>
  );
};

export default UserList;
