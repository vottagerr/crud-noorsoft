import React from "react";
import { Routes, Route } from "react-router-dom";

import "./App.css";
import UserList from "./components/UserList/UserList.js";
import AddUser from "./components/AddUser/AddUser.js";
// import { Home } from './components/Home';

function App() {
  return (
    <div className="app">
      <div className="container">
        <Routes>
          <Route path="/" element={<UserList />} />
          <Route path="/AddUser" element={<AddUser />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
